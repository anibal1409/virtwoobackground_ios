var exec = require('cordova/exec');
var channel = require("cordova/channel");

let nameClass = "Virtwoobackground";
let myOptions;
let myCredentials;
let myExtraData;
let subscribed = false;

module.exports = {
  
    initialize: function (credentials, extraData, options, cb) {
      myCredentials = credentials;
      myOptions = options;
      myExtraData = extraData;
      exec(cb, cb, nameClass, "initialize", [options, credentials, extraData]);
    },

    subscribeLocations: function(cb) {
      subscribed = true;
      this.getLastLocation(cb);
    },

    getLastLocation: function(cb) {
      exec(
        location => {
          if (subscribed) {
            cb(location);
            this.getLastLocation(cb);
          }
        },
        null,
        nameClass,
        "subscribeLocations",
        []
      );
    },

    unsubscribeLocations: function() {
      subscribed = false;
      exec(null, null, nameClass, "unsubscribeLocations", []);
    },

    socket: {
      emit: function(eventName, args, callback) {
        exec(
          success => {
            if (callback) {
              callback(null, success);
            }
          },
          error => {
            if (callback) {
              callback(error);
            }
          },
          nameClass,
          "emit",
          [eventName, ...args]
        );
      },
      on: function(eventName, callback) {
        exec(
          success => {
            console.log(success);
            callback(null, success);
          },
          error => {
            console.log(error);
            callback(error);
          },
          nameClass,
          "addSocketEventListener",
          [eventName]
        );
      }

    }
};

channel.deviceready.subscribe(function () {
 exec(null, null, nameClass, "deviceReady", []);
});
               
function execPromise(success, error, pluginName, method, args) {
  return new Promise(function (resolve, reject) {
    exec(
      function (result) {
        resolve(result);
      success
      },
      function (reason) {
          reject(reason);
          error
      },
      pluginName,
      method,
      args
    );
  });
}