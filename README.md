---
title: Backgroud Geolocation
description: Accede a los datos de ubicación y los transmite a través de socket  
---

# com.virtwoo.background

Este plugin accede a los datos de ubicación del dispositivo a través de un servicio que se ejecuta en segundo plano, opcionalmente los transmite a través de __websocket__ a una ruta de servidor que se especifíque en las opciones de configuración.

## Inicio rápido

1. Clonar el repositorio.
2. En aplicaciones desarrolladas con __Ionic__ ejecutar el comando

```sh
> ionic cordova plugin add [ruta a la carpeta del plugin]

```
3. Declarar cordova al inicio de la clase, también recomendable ejecutar los comandos del plugin una vez que se haya inicializado la plataforma

__Ejemplo__

```js

import { Platform } from '@ionic/angular';

...

declare var cordova: any;
@Injectable()
export class RealtimeLocationService {

    public createBackgroundService(): void { 
        this.platform.ready()
        .then(() => {
            ...
            // Iniciar en este punto las operaciones del plugin
            ...
        }

    }
    ...
```

4. Acceder a los métodos del plugin a través de `virtwoobackground`

__Ejemplo__

```js

import { Platform } from '@ionic/angular';

...

declare var cordova: any;
@Injectable()
export class RealtimeLocationService {

    public createBackgroundService(): void { 
        this.platform.ready()
        .then(() => {
            // Inicializa el servicio en segundo plano
            virtwoobackground.initialize(
            // Credenciales
            {
              userName: // Nombre de usuario de acceso
              token: // Access token de autorización
            },
            // Data extra 
            {
              id: 123,
              category: ...,
              ...
            },
            // Opciones de configuración
            {
              startOnDeviceBoot: true,
              socket: {
                autoConnect: true,
                host: '190.9.32.117',
                port: '3001',
                nsp: 'location'
              },
              notification: {
                title: 'Handshake project',
                text: 'Estamos actualizando tu posicion en tiempo real...',
              },
              location: {
                interval: 5000,
                priority: 1,
                distance: 10.0
              }
            },
            (error: any, result: any) => {
              if (error) {
                  console.log(error);
              } else {
                  console.log(result);
              }
            }
        );
        virtwooBackground.subscribeLocations(current => {
          // Manejar aquí las actualizaciones de posición del dispositivo
        });

        // Se obtiene una interfaz al socket nativo
        const socket = this.virtwooBackground.socket;
        socket.on('connect', (error: any, resultConnect: any) => {
          if (error) {
            ... 
          } else {
            ...
          }
        });
        socket.on('authenticated', (error: any, resultAuth: any) => {
          if (error) {
            console.error(error);
          } else {
              // manejar el evento de autenticación del usuario
          }
        });
    }
    }
    ...
```

## Plataformas soportadas

    - IOS

## Métodos

1. Constructor:  `virtwoobackground.initialize(CredencialesSocket, DataExtra, OpcionesConfiguracion, Callback(error, success))`

1.1 Parápetros (JSON):

- Credenciales:
    -userName
    - token
- extraData: Datos personalizados a ser enviados con la localización
- Opciones de configuración:
    - startOnDeviceBoot: Indica si el servicio se debe iniciar al arrancar el dispositivo
    - socket: Opciones de ruta del servidor socket
        - host
        - port
        - nsp: Name space
    - notification: Datos de la notificación a presentarse
        - title
        - text
        - icon
    - location:  Datos para la configuracion de actualizacion de la localizacion
      - interval
      - priority
      - distance
    
