//
//  LocationServices.swift
//
//  Created by Programmer Virtwoo on 2/5/20.
//

import Foundation
import CoreLocation
import SwiftyJSON

protocol LocationServiceDelegate {
  func tracingLocation(currentLocation: CLLocation)
  func tracingLocationDidFailWithError(error: NSError)
}

@available(iOS 8.0, *)
class LocationServices : NSObject, CLLocationManagerDelegate {
  
  private var locationManager = CLLocationManager()
  private var locationUpdate = false
  
  var lastLocation: CLLocation?
  var lastLocationStart = JSON.null
  private var delegate: LocationServiceDelegate?
  private var interval = 60
  private var distance = 10.0

  
  override init() {
    log("LocationServices init")
    super.init()
    locationManager.desiredAccuracy = kCLLocationAccuracyBest
    locationManager.delegate = self
    registerPermissions()
    if iOS8 {
      promptForNotificationPermission()
    }
  }
  
  func config() {
    do {
      let option = try AppDataManager.getInstance().getConfigOptions()
      interval = Int(option?.getLocation().getInterval() ?? 60)
      distance = option?.getLocation().getDistance() ?? 10.0
    } catch let err {
      print(err)
      interval = 60
      distance = 10.0
    }
    self.locationManager.allowsBackgroundLocationUpdates = true
    self.locationManager.distanceFilter = distance as CLLocationDistance
    self.locationManager.pausesLocationUpdatesAutomatically = false
//    self.locationManager.activityType = .fitness
    self.locationManager.allowDeferredLocationUpdates(untilTraveled: distance as CLLocationDistance, timeout: TimeInterval(interval))
    if #available(iOS 11.0, *) {
//      self.locationManager.showsBackgroundLocationIndicator = true //Mostrar notificacion de uso de la ubicacion en segundo plano
    } else {
      // Fallback on earlier versions
    }
  }

  func registerPermissions() {
    self.locationManager.requestAlwaysAuthorization()
  }
  
  func check () -> (Bool, [String], [[String:String]]) {
    var errors = [[String:String]]()
    var warnings = [String]()
    
    if (!enabled()) {
      errors.append([
          "code": "0",
          "message": "Locationservices disabled."
      ])
    }
    
    if (!authorization()) {
      errors.append([
          "code": "1",
          "message": "Location always permissions not granted."
      ])
    }
    if let notificationSettings = UIApplication.shared.currentUserNotificationSettings {
      print(notificationSettings.types.rawValue, "notificationSettings")
      if notificationSettings.types == .badge {
            errors.append([
                "code": "1",
                "message": "Notification permission missing"
            ])
        } else {
          if !notificationSettings.types.contains(.sound) {
                warnings.append("Warning: notification settings - sound permission missing")
            }
            
            if !notificationSettings.types.contains(.alert) {
                warnings.append("Warning: notification settings - alert permission missing")
            }
            
            if !notificationSettings.types.contains(.badge) {
                warnings.append("Warning: notification settings - badge permission missing")
            }
        }
    } else {
        errors.append([
            "code": "1",
            "message": "Notification permission missing"
        ])
    }
    let ok = (errors.count == 0)
    print(ok, warnings, errors)
    return (ok, warnings, errors)
    
  }
  
  func promptForNotificationPermission() {
    UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings.init(types: [UIUserNotificationType.sound, UIUserNotificationType.alert, UIUserNotificationType.badge], categories: nil))
  }
  
  func enabled() -> Bool {
    return CLLocationManager.locationServicesEnabled()
  }
  
  func  authorization() -> Bool{
    return CLLocationManager.authorizationStatus() ==  CLAuthorizationStatus.authorizedAlways
  }
  
  func startLocalitation() {
    
    self.locationManager.startUpdatingLocation()
    config()
    locationUpdate = true
  }
  
  func stopLocalitation() {
    self.locationManager.stopUpdatingLocation()
    locationUpdate = false
  }
  
  func Coord() -> JSON {
    var locationArray : JSON = JSON.null
    if (locationUpdate) {
      locationArray = map(locationManager.location)
    }
    if (lastLocationStart ==  JSON.null) {
      lastLocationStart = locationArray
    } else {
      locationArray =  lastLocationStart
    }
    return locationArray
  }
  
  func map(_ location: CLLocation?) -> JSON {
      var locationArray : JSON = JSON.null
        if (location != nil) {
          let formatter = DateFormatter()
          formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
          formatter.timeZone = NSTimeZone.local
          let timestampFormattedStr = formatter.string(from: (location!.timestamp))
          let timeZone = NSTimeZone(forSecondsFromGMT: NSTimeZone.local.secondsFromGMT())
          let timeZoneName = timeZone.localizedName(.standard, locale: NSLocale.current)!
          let timestampWithTimeZone = "\(timestampFormattedStr) \(timeZoneName)"
          locationArray = [
            "latitude" : location!.coordinate.latitude,
            "longitude" : location!.coordinate.longitude,
            "altitude" : location!.altitude,
            "course" : location!.course,
            "horizontalAccuracy" : location!.horizontalAccuracy,
            "verticalAccuracy" : location!.verticalAccuracy,
            "speed" : location!.speed,
            "time" : timestampWithTimeZone
          ]
        }
      return locationArray
    }
  
  func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
    switch(CLLocationManager.authorizationStatus()) {
    case .authorizedAlways:
      print(CLLocationManager.authorizationStatus().rawValue, "status", status.rawValue)
      NotificationCenter.default.post(name: .PERMISSION, object: true)
    case .denied, .restricted:
      stopLocalitation()
       print(CLLocationManager.authorizationStatus().rawValue, "status", status.rawValue)
      print("restricted")
      NotificationCenter.default.post(name: .PERMISSION, object: false)
    case .authorizedWhenInUse:
      print("authorizedWhenInUse")
    case .notDetermined:
      print("notDetermined")
    @unknown default:
      print("default")
    }
  }
  
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    for location in locations {
      if (Client.socket != nil && Client.socket!.status == .connected) {
        let locationMap = LocationMapper.mapLocationFields(map(location), true)
        Client.socket.emit("UPDATE_MY_LOCATION", locationMap.description)
        NotificationCenter.default.post(name: .PUBLICAR_DATOS, object: locationMap)
      }
    }
    self.lastLocation = locations.last!
    updateLocation(locations.last!)
  }
  
  func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
    updateLocationDidFailWithError(error as NSError)
  }
  
  private func updateLocation(_ currentLocation: CLLocation){
    guard let delegate = self.delegate else {
        return
    }
    delegate.tracingLocation(currentLocation: currentLocation)
  }

  private func updateLocationDidFailWithError(_ error: NSError) {
    guard let delegate = self.delegate else {
          return
    }

    delegate.tracingLocationDidFailWithError(error: error)
  }
  
}

