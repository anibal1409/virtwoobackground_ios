//
//  LocationMapper.swift
//
//  Created by Programmer Virtwoo on 2/7/20.
//

import Foundation
import SwiftyJSON

public class LocationMapper {
    
  private static var  appDataManager: AppDataManager = AppDataManager.getInstance()
  
  public init() {
  }
  
  public static func mapLocationFields( _ location: JSON,_ wrappInCoords: Bool) -> JSON {
    var position: JSON = JSON.null
    if (location != JSON.null) {
      if (wrappInCoords) {
        position = [
          "location": location,
          "timestamp": location["time"].stringValue
        ]
      } else {
        position = location
      }
      do {
        let extraData = try appDataManager.getExtraData()
        for item in extraData.dictionaryValue {
          position[item.key] = item.value
        }
      } catch let err {
        print(err)
      }
      
    }
    return position
  }
}

