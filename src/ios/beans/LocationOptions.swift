//
//  LocationOptions.swift
//
//  Created by Programmer Virtwoo on 2/6/20.
//

public class LocationOptions: Codable {

//  private var interval: Int
  private var interval: Double
  private var priority: Int
  private var distance: Double
  
  public init(_ interval: Double, _ priority: Int, _ distance: Double) {
    self.interval = interval
    self.priority = priority
    self.distance = distance
  }

  public func getInterval() -> Double {
    return interval
  }

  public func setInterval(_ interval: Double) {
    self.interval = interval
  }

  public func getPriority() -> Int {
    return priority
  }

  public func setPriority(_ priority: Int) {
    self.priority = priority
  }
  
  public func getDistance() -> Double {
    return distance
  }

  public func setDistance(_ distance: Double) {
    self.distance = distance
  }
  
}

