//
//  NotificationOptions.swift
//
//  Created by Programmer Virtwoo on 2/6/20.
//


public class NotificationOptions: Codable {
  
//  private static final long serialVersionUID = 1L
  private var title: String = "Virtwoo en background"
  private var text: String = "Servicio virtwoo"
  private var startAppOnTouch: Bool = false
  private var icon: String = "ic_launcher"
  
  public init() {
    
  }
  
  public init(_ title: String,_ text: String,_ icon: String,_ startAppOnTouch: Bool ) {
    self.startAppOnTouch = startAppOnTouch
    self.title = title
    self.text = text
    self.icon = icon
  }


  public func getIcon() -> String {
    return icon
  }

  public func isStartAppOnTouch() -> Bool {
    return startAppOnTouch;
  }

  public func setStartAppOnTouch(_ startAppOnTouch: Bool) {
    self.startAppOnTouch = startAppOnTouch
  }

  public func setIcon(_ icon: String) {
    self.icon = icon
  }

  public func getTitle() -> String {
    return title
  }

  public func setTitle(_ title: String) {
    self.title = title
  }

  public func getText() -> String {
    return text
  }

  public func setText(_ text: String) {
    self.text = text
  }
}
