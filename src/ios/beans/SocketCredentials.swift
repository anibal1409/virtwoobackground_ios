//
//  SocketCredentials.swift
//
//  Created by Programmer Virtwoo on 2/6/20.
//

struct SocketCredentials2: Codable {
    var token: String
    var userName: String
}

public class SocketCredentials: Codable {
  
  private var token: String
  private var userName: String

  public init(_ userName: String, _ token: String) {
    self.userName = userName
    self.token = token
  }

  public func getToken() -> String {
    return token
  }

  public func setToken(_ token: String) {
    self.token = token
  }

  public func getUserName() -> String {
    return userName
  }

  public func setUserName(_ userName: String) {
    self.userName = userName
  }

}
