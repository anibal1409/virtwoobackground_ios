//
//  ConfigOptions.swift
//
//  Created by Programmer Virtwoo on 2/6/20.
//

public class ConfigOptions: Codable {

  private var location: LocationOptions
  private var notification: NotificationOptions
  private var socket: SocketOptions
  private var startOnDeviceBoot: Bool = false;
  
  public init (_ location: LocationOptions, _ notification: NotificationOptions, _ socket: SocketOptions, _ startOnDeviceBoot: Bool) {
    self.location = location
    self.notification = notification
    self.socket = socket
    self.startOnDeviceBoot = startOnDeviceBoot
  }


  public func getNotification() -> NotificationOptions {
    return notification
  }

  public func getLocation() -> LocationOptions {
    return location
  }

  public func setLocation(_ location: LocationOptions) {
    self.location = location
  }

  public func setNotification(_ notification: NotificationOptions) {
    self.notification = notification
  }

  public func getSocket() -> SocketOptions {
    return socket
  }

  public func setSocket(_ socket: SocketOptions) {
    self.socket = socket
  }

  public func isStartOnDeviceBoot() -> Bool {
    return startOnDeviceBoot
  }

  public func setStartOnDeviceBoot(_ startOnDeviceBoot: Bool) {
    self.startOnDeviceBoot = startOnDeviceBoot
  }
  
}
