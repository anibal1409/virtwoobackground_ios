//
//  SocketOptions.swift
//
//  Created by Programmer Virtwoo on 2/6/20.
//

public class SocketOptions: Codable {
//    private var static final serialVersionUID: long = 1L
    private static var serialVersionUID: Int64 = 1 as Int64
    private var autoConnect: Bool = false
    private var host: String = "192.168.1.106" //"192.168.1.131" "192.168.1.106"
    private var nsp: String = ""
    private var port: String = "3000"
    
    public init () {
        
    }
    
    public init(_  host: String, _ port: String, _ nsp: String, _ autoConnect: Bool) {
      self.autoConnect = autoConnect
      self.host = host
      self.nsp = nsp
      self.port = port
    }

    public func getHost() -> String{
      return host
    }

    public func setHost(_  host: String) {
      self.host = host
    }

    public func isAutoConnect() -> Bool {
      return autoConnect
    }

    public func setAutoConnect(_ autoConnect: Bool) {
      self.autoConnect = autoConnect
    }

    public func getNsp() -> String {
      return nsp
    }

    public func setNsp(_ nsp: String) {
      self.nsp = nsp
    }

    public func getPort() -> String {
      return port
    }

    public func setPort(_ port: String) {
      self.port = port
    }
}

