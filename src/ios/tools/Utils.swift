//
//  utils.swift
//
//  Created by Programmer Virtwoo on 2/5/20.
//
import Foundation
import SwiftyJSON

let TAG = "backgroundV"
let iOS8 = floor(NSFoundationVersionNumber) > floor(NSFoundationVersionNumber_iOS_7_1)
let iOS7 = floor(NSFoundationVersionNumber) <= floor(NSFoundationVersionNumber_iOS_7_1)
private let decoder = JSONDecoder()
private let encoder = JSONEncoder()


typealias Callback = ([[String:String]]?) -> Void

struct Command {
    var json: JSON
    var callback: Callback
}

func log(_ message: String){
    NSLog("%@ - %@", TAG, message)
}

func log(_ messages: [String]) {
    for message in messages {
        log(message);
    }
}

func log(_ errors: [[String:String]]) {
    for error in errors {
        log("\(error["code"]) - \(error["message"])");
    }
}

func toJSON(_ stringJson: String) throws -> JSON {
  var newJSON = JSON.null
  if let data = stringJson.data(using: .utf8) {
    newJSON = try! JSON(data: data)
  }
  return newJSON
}

extension Notification.Name {
  static let updateLocation = Notification.Name("updateLocation")
  static let REGISTRAR_CLIENTE = Notification.Name("0")
  static let DESVINCULAR_CLIENTE = Notification.Name("1")
  static let PUBLICAR_DATOS = Notification.Name("2")
  static let FINALIZAR_SERVICIO = Notification.Name("3")
  static let CONNECT_WEB_SOCKET = Notification.Name("4")
  static let DISCONNECT_WEB_SOCKET = Notification.Name("5")
  static let PERMISSION = Notification.Name("PERMISSION")
}


func toConfigOptions(_ json: JSON) throws -> ConfigOptions? {
  var configOptions: ConfigOptions? = nil
  do {
    if (json != JSON.null) {
      configOptions = try decoder.decode(ConfigOptions.self, from: json.description.data(using: .utf8)!)
    }
  }
  return configOptions
}

func toSocketCredentials(_ json: JSON) throws -> SocketCredentials? {
  var socketCredentials: SocketCredentials? = nil
  do {
    if (json != JSON.null) {
      socketCredentials = try decoder.decode(SocketCredentials.self, from: json.description.data(using: .utf8)!)
    }
  }
  return socketCredentials
}

