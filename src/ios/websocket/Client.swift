//
//  Client.swift
//
//  Created by Programmer Virtwoo on 2/6/20.
//

import SocketIO

class Client {
  
  private static let TAG: String = "Socket.io"
  private static var socketManager: SocketManager!
  private static var mtoken: String!
  public static var socket: SocketIOClient!
  
  public init() {}
    

  public static func create(_ options: SocketOptions, _ credentials: SocketCredentials) -> SocketIOClient {
    let host = options.getHost()
    let port = options.getPort()
    let nsp = options.getNsp()
    self.mtoken = credentials.getToken()
    self.socketManager = SocketManager(socketURL: URL(string: "http://" + host + ":" + port + "/" + nsp)!, config: [.log(false), .compress])
    socket = self.socketManager.defaultSocket
    configSocket()
    return Client.socket
  }

  private static func configSocket() {
    Client.socket.on(clientEvent: SocketClientEvent.connect, callback: { _,_ in
      print(TAG + " Conectado....")
      authenticate()
    })
    Client.socket.on(clientEvent: SocketClientEvent.disconnect, callback: { _,_ in
      print(TAG  + " Desconectando")
    })
    Client.socket.on(clientEvent: SocketClientEvent.ping, callback: { _,_ in
        print(TAG  + " Ping")
    })
    Client.socket.on(clientEvent: SocketClientEvent.pong, callback: { _,_ in
        print(TAG  + " PONG")
    })
    Client.socket.on(clientEvent: SocketClientEvent.error, callback: { _,_ in
      print(TAG  + " Error de timeout")
    })
    Client.socket.on(clientEvent: SocketClientEvent.reconnect, callback: { _,_ in
      print(TAG  + " Reconectado")
    })
    Client.socket.on(clientEvent: SocketClientEvent.reconnectAttempt, callback: { _,_ in
      print(TAG  + " Intento de reconexión")
    })
    Client.socket.on(clientEvent: SocketClientEvent.websocketUpgrade, callback: { _,_ in
      print(TAG  + " Actualización de websocket")
    })
    Client.socket.on(clientEvent: SocketClientEvent.statusChange, callback: { _,_ in
      print(TAG  + " Cambio de estado")
    })
    Client.socket.on("authentication", callback: { _,_ in
      print(TAG  + " Autenticando")
    })
    
    Client.socket.on("chat message", callback: { _,_ in
      print(TAG  + " chat message")
    })
    
  }
  
  private static func authenticate() {
    Client.socket.emit("authentication", ["accessToken": mtoken])
  }

}
