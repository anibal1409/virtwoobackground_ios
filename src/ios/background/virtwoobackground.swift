import SwiftyJSON
import SocketIO

@objc(Virtwoobackground) @objcMembers  class Virtwoobackground : CDVPlugin {
  
  let nameClass = "Virtwoobackground"
  
  private lazy var locationServices = LocationServices()
  
  private var binded = false
  private var hasPermissions = false
  private var callbackLocation: CDVInvokedUrlCommand? = nil
   private var callbackPermission: CDVInvokedUrlCommand? = nil
  private var APP_PERMISSION_ACCESS_FINE_LOCATION = 1
  private var lastLocation = JSON.null

  private static var appDataManager: AppDataManager? = nil
  private static var configOptions: ConfigOptions? = nil
  private static var TAG = "VrtwooBPl"
  private static var gson = JSON.null
  private static var socket: SocketIOClient? = nil
  
  override func pluginInitialize () {
    
    NotificationCenter.default.addObserver(self, selector: #selector(Virtwoobackground.handleMessage(notfication:)), name: .PUBLICAR_DATOS, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(Virtwoobackground.updatePermission(notfication:)), name: .PERMISSION, object: nil)
  }
  
  func initialize(_ command: CDVInvokedUrlCommand) {
    print("Plugin initialization")
    self.locationServices.registerPermissions()
    let result: CDVPluginResult
    if (command.arguments != nil && command.arguments.count != 0) {
      do {
          let configOptionsJSON = JSON(command.arguments![0])
          let socketCredentialsJSON = JSON(command.arguments![1])
          if (configOptionsJSON == JSON.null || socketCredentialsJSON == JSON.null) {
            result = CDVPluginResult(
              status: CDVCommandStatus_ERROR,
              messageAs: "Expected one non-null string argument."
            )
          } else {
            Virtwoobackground.appDataManager =  AppDataManager.getInstance();
            Virtwoobackground.configOptions = try toConfigOptions(configOptionsJSON)
            let credentialsLocal = try toSocketCredentials(socketCredentialsJSON)
            try Virtwoobackground.appDataManager!.storeConfigData(Virtwoobackground.configOptions!)
            try Virtwoobackground.appDataManager!.storeUserdata(credentialsLocal!)
            try Virtwoobackground.appDataManager!.storeExtraData(JSON(command.arguments![2]))
            Virtwoobackground.socket = Client.create(Virtwoobackground.configOptions!.getSocket(), credentialsLocal!)
            if (locationServices.authorization()) {
              result = self.startRealTimeLocationsUpdate()
              self.commandDelegate!.send(result, callbackId: command.callbackId)
            } else {
              callbackPermission = command
            }
          }
      } catch let err {
        print(self.nameClass, err)
        result = CDVPluginResult(
          status: CDVCommandStatus_ERROR,
          messageAs: err.localizedDescription
        )
        self.commandDelegate!.send(result, callbackId: command.callbackId)
      }
    } else {
      result = CDVPluginResult(
        status: CDVCommandStatus_ERROR,
        messageAs: "Expected one non-empty string argument."
      )
      self.commandDelegate!.send(result, callbackId: command.callbackId)
    }
    
  }
  
  private func startRealTimeLocationsUpdate() -> CDVPluginResult {
    let result: CDVPluginResult
    let (ok, warnings, errors) = self.locationServices.check()
    hasPermissions = ok
    print(hasPermissions)
    if (!hasPermissions) {
      result = CDVPluginResult(
        status: CDVCommandStatus_ERROR,
        messageAs: "You do not have a location permit"
      )
    } else {
      startAndBindService()
      result = CDVPluginResult(
        status: CDVCommandStatus_OK,
        messageAs: "Started service"
      )
    }
//    self.commandDelegate!.send(result, callbackId: command.callbackId)
    return result
  }
  
  private func startAndBindService() {
    DispatchQueue.global(qos: .userInitiated).async {
      DispatchQueue.main.async {
        if ( self.hasPermissions) {
          self.locationServices.startLocalitation()
          if (Virtwoobackground.configOptions!.getSocket().isAutoConnect()) {
            print("socket!.connect()")
            Virtwoobackground.socket!.connect()
          }
        }
      }
    }
  }
  
  public func getLastLocation(_ command: CDVInvokedUrlCommand) {
    self.commandDelegate!.send(
      CDVPluginResult(
        status: CDVCommandStatus_OK,
        messageAs: lastLocation.description
      ),
      callbackId: command.callbackId
    )
  }
  
  public func subscribeLocations(_ command: CDVInvokedUrlCommand) {
    callbackLocation = command
  }
  
  public func unsubscribeLocations(_ command: CDVInvokedUrlCommand) {
    callbackLocation = nil
  }
  
  public func addSocketEventListener(_ command: CDVInvokedUrlCommand) {
    DispatchQueue.global(qos: .userInitiated).async {
      DispatchQueue.main.async {
        var result: CDVPluginResult
        if (command.arguments.count > 0) {
          if (Virtwoobackground.socket != nil && Virtwoobackground.socket!.status == .connected) {
            Virtwoobackground.socket!.on(command.arguments![0] as! String, callback: { args,ACK in
              let myResult = CDVPluginResult(
                status: CDVCommandStatus_OK,
                messageAs: JSON(args[0]).description
              )
              self.commandDelegate!.send(myResult, callbackId: command.callbackId)
            })
          } else {
            result = CDVPluginResult(
              status: CDVCommandStatus_ERROR,
              messageAs: "Service not started"
            )
            self.commandDelegate!.send(result, callbackId: command.callbackId)
          }
        } else {
          result = CDVPluginResult(
            status: CDVCommandStatus_ERROR,
            messageAs: "No event name"
          )
          self.commandDelegate!.send(result, callbackId: command.callbackId)
        }
      }
    }
  }
  
  public func emit(_ command: CDVInvokedUrlCommand) {
    if (command.arguments.count > 0) {
      print("emit")
      if (Virtwoobackground.socket != nil && Virtwoobackground.socket!.status == .connected) {
        Virtwoobackground.socket!.emitWithAck(command.arguments![0] as! String, command.arguments![1] as! String).timingOut(after: 1) { args in
          var newObject: Any? = nil
          if (args != nil) {
            if (args.count == 1) {
              newObject = args[0]
              self.commandDelegate!.send(CDVPluginResult(status: CDVCommandStatus_OK, messageAs: (newObject! as! String) ), callbackId: command.callbackId)
            } else if (args.count == 2){
              newObject = args[0]
              if (newObject == nil) {
                self.commandDelegate!.send(CDVPluginResult(status: CDVCommandStatus_OK, messageAs: JSON(newObject!).arrayValue), callbackId: command.callbackId)
              }
            } else if (args.count == 3) {
              self.commandDelegate!.send(CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: (args[2] as! String)), callbackId: command.callbackId)
            }
          }
        }
      }
    }
  }

  func deviceReady(_ command: CDVInvokedUrlCommand) {
    print("deviceReady")
    let pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: "deviceReady")
    commandDelegate!.send(pluginResult, callbackId: command.callbackId)
  }

  @objc func handleMessage(notfication: NSNotification) {
    lastLocation = JSON.null
    let location = JSON(notfication.object!)
    if (location != JSON.null) {
      lastLocation = location
    }
    if (callbackLocation != nil) {
      self.commandDelegate!.send(CDVPluginResult(status: CDVCommandStatus_OK, messageAs: location.description), callbackId: callbackLocation?.callbackId)
    }
  }
  
  @objc func updatePermission(notfication: NSNotification) {
    var result: CDVPluginResult
    self.hasPermissions = notfication.object! as! Bool
    if (!self.hasPermissions) {
      result = CDVPluginResult(
        status: CDVCommandStatus_ERROR,
        messageAs: "You do not have a location permit..."
      )
    } else {
      self.startAndBindService()
      result = CDVPluginResult(
        status: CDVCommandStatus_OK,
        messageAs: "Started service"
      )
    }
    if (self.callbackPermission != nil) {
      self.commandDelegate!.send(result, callbackId: self.callbackPermission!.callbackId)
    }
  }

  
}
