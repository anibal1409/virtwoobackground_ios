//
//  AppDataManager.swift
//
//  Created by Programmer Virtwoo on 2/10/20.
//

import Foundation
import SwiftyJSON

public class AppDataManager {
    
  private static var appDataManager: AppDataManager? = nil
  
  private let decoder = JSONDecoder()
  private let encoder = JSONEncoder()
  private static var USER_DATA_FILE = "userData"
  private static var CONFIG_DATA_FILE = "configData"
  private static var EXTRA_DATA_FILE = "extraData"
  
  private var fileManager = FileManagerService()
  private var json: JSON = JSON.null
  
  private var extraData: JSON = JSON.null
  private var socketCredentials: SocketCredentials?
  private var configOptions: ConfigOptions?
  
  private init() {
  }
  
  public static func getInstance() -> AppDataManager {
    if (self.appDataManager == nil) {
      self.appDataManager = AppDataManager()
    }
    return self.appDataManager!
  }
  
  public func getExtraData() throws -> JSON {
    if (self.extraData == JSON.null) {
      do {
        let content = try self.fileManager.readDataFile(AppDataManager.EXTRA_DATA_FILE)
        if (content != "") {
          self.extraData = try toJSON(content)
        }
      }
    }
    return self.extraData
  }

  public func storeExtraData(_ extraData: JSON) throws {
    do {
      self.extraData = extraData
      try self.fileManager.storeDataFile(AppDataManager.EXTRA_DATA_FILE, extraData.description)
    }
  }

  public func getUserData() throws -> SocketCredentials? {
    if (self.socketCredentials == nil) {
      do {
        let content = try self.fileManager.readDataFile(AppDataManager.USER_DATA_FILE)
        if (content != "") {
          self.socketCredentials = try self.decoder.decode(SocketCredentials.self, from: content.data(using: .utf8)!)
        }
      }
    }
    return self.socketCredentials
  }

  public func storeUserdata(_ userData: SocketCredentials) throws {
    self.socketCredentials = userData
    do {
      let data = try self.encoder.encode(userData)
      let string = String(data: data, encoding: .utf8)!
      try self.fileManager.storeDataFile(AppDataManager.USER_DATA_FILE, string)
    }
  }

  public func storeConfigData(_ options: ConfigOptions) throws {
    self.configOptions = options
    do {
      let data = try self.encoder.encode(options)
      let string = String(data: data, encoding: .utf8)!
      try self.fileManager.storeDataFile(AppDataManager.CONFIG_DATA_FILE, string)
    } catch let err {
      print("self.nameClass", err)
    }
  }

  public func getConfigOptions() throws -> ConfigOptions? {
    if (self.configOptions == nil) {
      do {
        let content = try self.fileManager.readDataFile(AppDataManager.CONFIG_DATA_FILE)
        if (content != "") {
          self.configOptions = try self.decoder.decode(ConfigOptions.self, from: content.data(using: .utf8)!)
        }
      }
    }
    return self.configOptions
  }
  
}
