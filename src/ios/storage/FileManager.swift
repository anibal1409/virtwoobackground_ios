//
//  FileManagerService.swift
//
//  Created by Programmer Virtwoo on 2/10/20.
//

import Foundation

public class FileManagerService {
  
  public init() {
  }
  
func readDataFile(_ fileName: String) throws -> String {
  var response = ""
  if (fileName != "") {
    if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {

      let path = dir.appendingPathComponent(fileName)

      do {
        response = try String(contentsOf: path, encoding: String.Encoding.utf8)
      }
    }
  }
  return response
}
  
  func storeDataFile(_ fileName: String, _ userData: String) throws -> Bool {
    var response = false
    if (fileName != "" && userData != "") {
      if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {

        let path = dir.appendingPathComponent(fileName)
        do {
          try userData.write(to: path, atomically: false, encoding: String.Encoding.utf8)
          response = true
        }
      }
    }
    return response
  }
  
}

